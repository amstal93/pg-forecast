package com.icaro.paraglidingforecastbackend.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@EnableConfigurationProperties({VisualCrossingClientConfiguration.class, WeatherApiClientConfiguration.class})
public class AppConfiguration {
}
