package com.icaro.paraglidingforecastbackend.mappers.weatherApi;

import com.icaro.paraglidingforecastbackend.dto.WeatherApi.WeatherApiForecastDay;
import com.icaro.paraglidingforecastbackend.models.forecast.ForecastDay;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

@Mapper(
    componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.ERROR,
    uses = {WeatherApiHourMapper.class})
public interface WeatherApiDayMapper {
  @Mapping(target = "score", ignore = true)
  @Mapping(target = "sunrise", source = "astro.sunrise")
  @Mapping(target = "sunset", source = "astro.sunset")
  ForecastDay toForecastDay(WeatherApiForecastDay weatherApiForecastDay);
}
