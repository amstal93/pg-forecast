package com.icaro.paraglidingforecastbackend.dto.VisualCrossing;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class VisualCrossingDay {
  private final LocalDate date;

  private final Double tempMax;
  private final Double tempMin;
  private final Double temp;

  private final Double dew;
  private final Double humidity;
  private final Double precipitation;
  private final Double precipProb;

  private final Double windGust;
  private final Double windSpeed;
  private final Double windDir;

  private final Double cloudCover;
  private final Double visibility;
  private final Double severityRisk;

  private final LocalTime sunrise;
  private final LocalTime sunset;

  private final VisualCrossingHour[] hours;

  public VisualCrossingDay(
      @JsonProperty("datetime") final String date,
      @JsonProperty("sunrise") final String sunrise,
      @JsonProperty("sunset") final String sunset,
      @JsonProperty("tempmax") final Double tempMax,
      @JsonProperty("tempmin") final Double tempMin,
      @JsonProperty("temp") final Double temp,
      @JsonProperty("dew") final Double dew,
      @JsonProperty("humidity") final Double humidity,
      @JsonProperty("precip") final Double precipitation,
      @JsonProperty("precipprob") final Double precipProb,
      @JsonProperty("windgust") final Double windGust,
      @JsonProperty("windspeed") final Double windSpeed,
      @JsonProperty("winddir") final Double windDir,
      @JsonProperty("cloudcover") final Double cloudCover,
      @JsonProperty("visibility") final Double visibility,
      @JsonProperty("severerisk") final Double severityRisk,
      @JsonProperty("hours") VisualCrossingHour[] hours) {
    this.date = LocalDate.parse(date);
    this.sunrise = LocalTime.parse(sunrise);
    this.sunset = LocalTime.parse(sunset);
    this.tempMax = tempMax;
    this.tempMin = tempMin;
    this.temp = temp;
    this.dew = dew;
    this.humidity = humidity;
    this.precipitation = precipitation;
    this.precipProb = precipProb;
    this.windGust = windGust;
    this.windSpeed = windSpeed;
    this.windDir = windDir;
    this.cloudCover = cloudCover;
    this.visibility = visibility;
    this.severityRisk = severityRisk;
    this.hours = hours;
  }
}
