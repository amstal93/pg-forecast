package com.icaro.paraglidingforecastbackend.models.location;

public enum LocationType {
  DYNAMIC,
  THERMAL,
  GROUND_HANDLING,
  LANDING,
  UNKNOWN
}
