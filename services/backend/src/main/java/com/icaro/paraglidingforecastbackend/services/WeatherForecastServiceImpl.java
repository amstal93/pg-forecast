package com.icaro.paraglidingforecastbackend.services;

import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;
import com.icaro.paraglidingforecastbackend.repositories.LocationRepository;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class WeatherForecastServiceImpl implements WeatherForecastService {

  final LocationRepository locationRepository;
  final MongoTemplate mongoTemplate;
  final List<WeatherModelService> modelServices;

  public WeatherForecastServiceImpl(
      LocationRepository locationRepository,
      MongoTemplate mongoTemplate,
      List<WeatherModelService> modelServices) {
    this.locationRepository = locationRepository;
    this.mongoTemplate = mongoTemplate;
    this.modelServices = modelServices;
  }

  @Override
  public Map<String, List<Forecast>> getForecastsByModel() {
    Map<String, List<Forecast>> forecasts = new HashMap<>();

    for (WeatherModelService model : modelServices) {
      // TODO: there's probably a better way to do this
      String serviceName = model.getClass().getName().split("WeatherModelService")[1];
      forecasts.put(serviceName, mongoTemplate.findAll(Forecast.class, serviceName));
    }

    return forecasts;
  }
}
