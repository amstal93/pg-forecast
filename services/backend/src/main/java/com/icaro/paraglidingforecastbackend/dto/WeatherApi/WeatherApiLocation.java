package com.icaro.paraglidingforecastbackend.dto.WeatherApi;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
public class WeatherApiLocation {
  private final String name;
  private final String region;
  private final String country;
  private final double lat;
  private final double lon;

  public WeatherApiLocation(
      @JsonProperty("name") final String name,
      @JsonProperty("region") final String region,
      @JsonProperty("country") final String country,
      @JsonProperty("lat") final double lat,
      @JsonProperty("lon") final double lon) {
    this.name = name;
    this.region = region;
    this.country = country;
    this.lat = lat;
    this.lon = lon;
  }
}
