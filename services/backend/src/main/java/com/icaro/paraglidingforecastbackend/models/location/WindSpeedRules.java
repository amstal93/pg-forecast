package com.icaro.paraglidingforecastbackend.models.location;

import lombok.Getter;

import java.security.InvalidParameterException;

@Getter
public class WindSpeedRules {
  final int minimumVelocity;
  final int maximumVelocity;

  public WindSpeedRules(final int minimumVelocity, final int maximumVelocity) {
    if (minimumVelocity < 0) {
      throw new InvalidParameterException("Wind speed must be at least 0");
    }
    if (maximumVelocity < minimumVelocity) {
      throw new InvalidParameterException(
          "Maximum wind speed must be equal or greater than minimum velocity");
    }
    this.minimumVelocity = minimumVelocity;
    this.maximumVelocity = maximumVelocity;
  }
}
