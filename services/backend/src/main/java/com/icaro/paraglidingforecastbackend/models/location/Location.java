package com.icaro.paraglidingforecastbackend.models.location;

import lombok.Data;
import lombok.NonNull;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "Locations")
public final class Location {
    @NonNull
    private final String name;

    @NonNull
    private final Coordinate coordinates;

    private final String picture;

    private final LocationType locationType;

    @NonNull
    private final LocationRules locationRules;
}
