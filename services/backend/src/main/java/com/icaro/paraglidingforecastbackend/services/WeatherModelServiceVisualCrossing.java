package com.icaro.paraglidingforecastbackend.services;

import com.icaro.paraglidingforecastbackend.clients.VisualCrossingClient;
import com.icaro.paraglidingforecastbackend.mappers.visualCrossing.VisualCrossingMapper;
import com.icaro.paraglidingforecastbackend.models.forecast.Forecast;
import com.icaro.paraglidingforecastbackend.models.location.Location;
import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WeatherModelServiceVisualCrossing implements WeatherModelService {

  final VisualCrossingClient visualCrossingClient;
  final VisualCrossingMapper visualCrossingMapper;

  public WeatherModelServiceVisualCrossing(
      VisualCrossingClient visualCrossingClient, VisualCrossingMapper visualCrossingMapper) {
    this.visualCrossingClient = visualCrossingClient;
    this.visualCrossingMapper = visualCrossingMapper;
  }

  public List<Forecast> getForecasts(List<Location> locations) {
    List<Forecast> forecasts = new ArrayList<>();
    for (Location location : locations) {
      forecasts.add(
          visualCrossingMapper.toForecast(
              new DateTime(),
              visualCrossingClient.getForecast(location.getCoordinates().toString()),
              location));
    }

    return forecasts;
  }

  public Forecast getForecast(Location location) {
    return visualCrossingMapper.toForecast(
        new DateTime(),
        visualCrossingClient.getForecast(location.getCoordinates().toString()),
        location);
  }
}
