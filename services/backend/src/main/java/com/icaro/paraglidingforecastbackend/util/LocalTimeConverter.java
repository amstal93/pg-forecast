package com.icaro.paraglidingforecastbackend.util;

import org.joda.time.LocalTime;
import org.springframework.core.convert.converter.Converter;

public class LocalTimeConverter implements Converter<LocalTime, String> {

  @Override
  public String convert(LocalTime source) {
    return source.toString();
  }
}
