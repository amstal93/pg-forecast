package com.icaro.paraglidingforecastbackend.dto.WindGuru;

import lombok.Data;

@Data
public class WindGuruDto {
  private final String model;
  private final String date;
  private final String windSpeed;
  private final String windGust;
  private final String windDir;
  private final String temp;
  private final String cloudCover;
  private final String precipitation;
}
