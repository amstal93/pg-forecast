import React from "react";
import styles from './table-element.module.css'
import {ForecastDay} from "../../../../dto/LocationForecast";
import TableDay from "../table-day/table-day";

interface IProps {
    days: ForecastDay[],
    expanded: boolean,
}

const TableElement: React.FunctionComponent<IProps> = (props) => {
    const {
        days,
        expanded,
    } = props;

    const date_label = 'Day | Score';
    const labels: { persistent: string[], collapsible: string[] } = {
        persistent: [
            "Hour",
            "Score",
        ],
        collapsible: [
            "Wind Speed",
            "Wind Gust",
            "Wind Direction",
            "Temperature",
            "Precipitation",
            "Humidity",
            "Cloud Cover (L)",
            "Cloud Cover (M)",
            "Cloud Cover (H)",
            "Visibility"
        ]
    }

    return (
        <div className={styles.forecastDaysHolder}>

            <ul className={styles.forecastDaysLabels}>

                <li className={styles.forecastDayLabel}>{date_label}</li>

                {labels.persistent.map(label => <li className={styles.forecastDayLabel} key={label}>{label}</li>)}

                {
                    expanded &&
                    labels.collapsible.map(label =>
                        <li className={styles.forecastDayLabel} key={label}>{label}</li>)
                }

            </ul>

            <div className={styles.forecastDaysForecast}>

                {days.map(day => <TableDay expanded={expanded} day={day} key={day.date}/>)}

            </div>

        </div>
    );
}

export default TableElement;