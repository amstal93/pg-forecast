import React, {useEffect} from 'react';
import LoadingSpinner from "../../loading-spinner/loading-spinner";
import {useDispatch, useSelector} from "react-redux";
import {
    loadAllForecasts,
    selectForecast,
    selectForecastModels,
    selectForecastsLoading,
    selectSelectedModel
} from "../../../store/reducers/models-forecast";
import {ModelsForecast} from "../../../dto/LocationForecast";
import ApiError from "../../api-error/api-error";
import {PgClientStatus} from "../../../services/pg-client";
import Forecast from "./forecast/forecast";

const ForecastPage: React.FunctionComponent = () => {
    const dispatch = useDispatch();
    const modelsForecast: ModelsForecast = useSelector(selectForecast);
    const models: string[] = useSelector(selectForecastModels);
    const selectedModel: string | null = useSelector(selectSelectedModel);
    const forecastServiceState: PgClientStatus = useSelector(selectForecastsLoading);

    useEffect(() => {
        dispatch(loadAllForecasts());
    }, [dispatch])

    switch (forecastServiceState) {
        case PgClientStatus.loading:
        case PgClientStatus.notStarted:
            return <LoadingSpinner/>;
        case PgClientStatus.error:
            return <ApiError/>;
        case PgClientStatus.done:
            if (selectedModel != null)
                return <Forecast modelsForecast={modelsForecast} models={models} selectedModel={selectedModel}/>;
            else
                return <ApiError/>
    }
}

export default ForecastPage;
