import {createSlice, PayloadAction} from "@reduxjs/toolkit"
import {ModelsForecast} from "../../dto/LocationForecast";
import PgClient, {PgClientStatus} from "../../services/pg-client";


interface ModelsForecastState {
    pgClientStatus: PgClientStatus,
    forecast: ModelsForecast,
    models: string[],
    selectedModel: string | null,
}

const initialState = {
    pgClientStatus: PgClientStatus.notStarted,
    forecast: {},
    models: [],
    selectedModel: null,
} as ModelsForecastState;


const pgClient = new PgClient();

const slice = createSlice({
    name: "forecast",
    initialState,
    reducers: {
        setForecastsAndModels: (state: ModelsForecastState, action: PayloadAction<ModelsForecast>) => {
            state.forecast = action.payload;
            const models = Object.keys(action.payload);
            state.models = models;

            if ((models && (!state.selectedModel || !models.includes(state.selectedModel)))) {
                state.selectedModel = models[0];
            }
        },

        setSelectedModel: (state: ModelsForecastState, action: PayloadAction<string>) => {
            const newSelectedModel = action.payload;

            if (state.models?.includes(newSelectedModel)) {
                state.selectedModel = newSelectedModel;
            }
        },

        setIsLoading: (state: ModelsForecastState, action: PayloadAction<PgClientStatus>) => {
            state.pgClientStatus = action.payload;
        },
    },
});

const {setForecastsAndModels, setIsLoading} = slice.actions;

// ACTIONS

export const {setSelectedModel} = slice.actions;

export const loadAllForecasts = () => (dispatch: Function) => {
    dispatch(setIsLoading(PgClientStatus.loading));

    pgClient.getAllForecasts().then(response => {
        dispatch(setForecastsAndModels(response.data));
        dispatch(setIsLoading(PgClientStatus.done));
    })
        .catch(error => {
            console.error(error);
            dispatch(setIsLoading(PgClientStatus.error));
        })
}

// SELECTORS

export const selectForecast = (state: { modelsForecast: ModelsForecastState }) => state.modelsForecast.forecast;
export const selectForecastModels = (state: { modelsForecast: ModelsForecastState }) => state.modelsForecast.models;
export const selectSelectedModel = (state: { modelsForecast: ModelsForecastState }) => state.modelsForecast.selectedModel;
export const selectForecastsLoading = (state: { modelsForecast: ModelsForecastState }) => state.modelsForecast.pgClientStatus;

export default slice.reducer;